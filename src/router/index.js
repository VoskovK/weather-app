import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Cities',
    component: () => import('../components/Cities.vue')
  },
  {
    path: '/city/:id/:name',
    name: 'CityInformation',
    component: () => import('../components/CityInformation.vue')
  },
  {
    path: '/create',
    name: 'CreateCity',
    component: () => import('../components/CreateCity.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
