import axios from "axios";
import { API_CITY_URL } from "../constants";

const apiClient = axios.create({
    baseURL: API_CITY_URL,
    withCredentials: false,
    headers: { 
        Accept: "application/json",
        "Content-Type": "application/json"
    }
})

export default { 
    
    getCity(cityName) {
        return apiClient.get('', {
            params: {
                q: cityName
            }
        });
    }
}