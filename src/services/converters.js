export function convertKelvinToCelsius(kelvinDegree) {
    return (kelvinDegree - 273.15).toFixed(1);
}