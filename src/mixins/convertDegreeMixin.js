import {convertKelvinToCelsius} from '@/services/converters.js'

export default {
    methods: {
        convertDegree(degree) {
            return convertKelvinToCelsius(parseFloat(degree));
        }
    }
}