import Vue from 'vue'
import App from './App.vue'
import store from './store'
import router from './router'
import VueMaterial from 'vue-material'

import 'bootstrap'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'

Vue.config.productionTip = false
Vue.use(VueMaterial)

store.dispatch('setupCities');

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
