import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import VueAxios from "vue-axios";

import service from "@/services/service.js";
import { getCookie, setCookie } from '../services/cookie-helper';

Vue.use(Vuex);
Vue.use(VueAxios, axios);

export default new Vuex.Store({
  state: {
    cities: [],
    dataIsRecived: false
  },

  mutations: {

    SET_CITIES(state, cities) {
        state.cities = cities;
    },

    ADD_CITY(state, cityData) {
      let cityIndex = state.cities.findIndex(c => c.id === cityData.id);

      if (cityIndex < 0) {
        state.cities.push(cityData)
      }

    },

    DELETE_CITY(state, id) { 
      let index = state.cities.findIndex(c => c.id === id);

      if (index > -1) {
        state.cities.splice(index, 1);
      }
    },

    UPDATE_CITY(state, cityData) { 
      let index = state.cities.findIndex(c => c.id === cityData.id);

      if (index > -1) {
        state.cities.splice(index, 1, cityData);
      } else { 
        state.cities.push(cityData);
      }
    }
  },

  actions: {

    setupCities({commit, state}) {
      let cities;

      try {
        cities = JSON.parse(getCookie('cities'))
      } catch {
        cities = [];
      }
      
      cities.forEach((city, index) => {
        service.getCity(city.name).then(r => { 
          cities[index] = r.data;
        });
      });
      
      commit('SET_CITIES', cities);
      setCookie('cities', JSON.stringify(state.cities));
    },

    addCity({commit, state}, city) {
      commit('ADD_CITY', city);
      setCookie('cities', JSON.stringify(state.cities));
    },

    deleteCity({commit, state}, id) { 
      commit('DELETE_CITY', id);
      setCookie('cities', JSON.stringify(state.cities));
    },

    updateCity({commit, state}, cityData) { 
      commit('UPDATE_CITY', cityData);
      setCookie('cities', JSON.stringify(state.cities));
    }
  },

  getters: { 
    cityById: s => id => s.cities.find(c => c.id === id)
  }
})
